$(document).on("click",".saludo", function(){

    var buttonVal = $(this).val();
    var text = $("#nombre").val();

    if (validate(text)){

        if (buttonVal=="Hola"){
            alert(buttonVal +" "+ text);
        }
        else if (buttonVal=="Adiós"){
            alert(buttonVal +" "+ text);
        }
    }
});

function validate(text){

    var patron = /^[a-zA-Z]*$/;

    if (text==""){
        errorMessage("Rellene el campo de nombre para continuar.");
        return false;
    }

    else if(text.includes(" ")){
        errorMessage("No se admiten espacios.");
        return false;
    }

    else if(!patron.test(text)){
        errorMessage("Solo se admiten letras");
        return false;
    }

    else{
        return true;
    }
}

function errorMessage(message){

    $("#errorWindow").text(message);
    $("#errorWindow").addClass("invalid");
    $("#nombre").addClass("invalid");

    setTimeout(function(){
        $("#nombre").removeClass("invalid");
        $("#errorWindow").removeClass("invalid");
        $("#errorWindow").text("");
    }, 1000);


}